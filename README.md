# VlaDB (pronounced FlaaaaDB)
Creates a flat file data storage via given converters.

## Example
A converter looks like this:

```javascript
const data = require("./some.json")
const vladb = require("vladb")

const productIdEndpoint = {
  endpoint: "db/product/",
  generator: data => data.products,
  id: product => `${product.id}`,
  statistics: products => ({
    count: products.length,
  })
}

var unique = data => data.filter((v, i, a) => a.indexOf(v) === i); 
const getCategory = product => product.category

const productCategoryEndpoint = {
  endpoint: "db/productByCategory/",
  generator: data => data.products,
  id: product => `${product.category}`,
  statistics: products => ({
    count: unique(products.map(getCategory)).length
  })
}

vladb.create([ productIdEndpoint, productCategoryEndpoint ], data, (err) => {
  if (err) {
    console.log("Error happend", err)
    return;
  }

  console.log("success")
})
```
