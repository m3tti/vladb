const {
  __,
  pipe,
  prop,
  groupBy,
  length,
  values,
  assoc,
} = require("ramda")
const vladb = require("./engine")

const data = {
  bla: [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
  ]
}

const conv = {
  endpoint: "db/data/",
  generator: prop("bla"), 
  id: prop("id"),
  statistics: pipe(
    values,
    length,
    assoc("count", __, {})
  ),
}

vladb.create([conv], data, (err, data) => console.log(err, data))
