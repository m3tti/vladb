const { 
  curry,
} = require("ramda")

const {
  asyncify,
  map,
  parallel,
  ensureAsync,
} = require("async")

const {
  writeFile,
  mkdir,
} = require("fs")

const createDBEntry = curry((endpoint, idfn, doc, cb) => {
  writeFile(
    `${endpoint}/${idfn(doc)}`, 
    JSON.stringify(doc),
    cb
  )
})

const generateStatistics = curry((endpoint, statisticsFn, docs, cb) => {
  writeFile(
    `${endpoint}/statistics`,
    JSON.stringify(statisticsFn(docs)),
    cb
  )
})

const prepareDocs = curry((data, converter, cb) => {
  mkdir(converter.endpoint, { recursive: true }, (err) => {
    if (err) cb(err, null)

    let docs = converter.generator(data)

    let entries = cb => map(docs, createDBEntry(converter.endpoint, converter.id), cb)
    let createStatistics = cb => generateStatistics(converter.endpoint, converter.statistics, docs, cb)

    parallel([ensureAsync(entries), ensureAsync(createStatistics)], cb)
  })
})

/** Creates the flatt file database with the help of the provided
  * converter functions
  *
  * @converter = [{ 
  *   generator: data => array,
  *   id: doc => string
  *   endpoint: string
  *   statistics: docs => object
  * }]
  */
const create = curry((converter, data, cb) => 
  map(converter, prepareDocs(data), cb)
)

const spy = curry((str, data) => {
  console.log(str, data)
  return data
})

module.exports = {
  create
}
